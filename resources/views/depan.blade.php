@extends('includes.layout')
@section('content')
    <main role="main">

        <div class="container-fluid">
            <section id="beranda">
                <div class="row">
                    <div class="col-12">
                        <img class="" style=" width: 100%; height: 750px" src="{{ asset('img/farm.jpg') }}">
                        <h1 style="position: absolute;transform: translate(-50%,-50%); bottom: 57%; left: 50%;"
                            class="text-bold">Lets Make Your Dream in Here</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <!-- Three columns of text below the carousel -->
                    <div class="row py-5">
                        <h1 class="text-center col-12 py-5 text-black font-weight-bolder">#Trending Produk</h1>
                        @foreach ($produks as $produk)
                            <div class="col-md-4 py-3">
                                <div class="card">
                                    @if ($produk->jenis == 'Kambing')
                                        <img src="{{ asset('img/kambingg.jpg') }}">
                                    @elseif($produk->jenis == 'Domba')
                                        <img src="{{ asset('img/domba.jpg') }}">
                                    @else
                                        <img src="{{ asset('img/sapi.jpg') }}">
                                    @endif

                                    <div class="card-body">
                                        <h5> {{ $produk->jenis }} {{ $produk->gender }} </h5>
                                        <p> {{ number_format($produk->hargaJual) }} </p>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-outline-info col-5" data-toggle="modal"
                                            data-target="#modal-detail-{{ $produk->id }}">
                                            Lihat detail
                                        </button>
                                        <button class="btn btn-outline-success col-5"> Beli </button>
                                        <div class="modal fade" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true"
                                            id="modal-detail-{{ $produk->id }}">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Detail {{ $produk->jenis }}
                                                            {{ $produk->gender }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Nama : {{ $produk->jenis }}</p>
                                                        <p>Type : {{ $produk->type }}</p>
                                                        <p>Berat : {{ $produk->wight }} kg</p>
                                                        <p>Tinggi : {{ $produk->hight }} cm</p>
                                                        <p>Jenis Kelamin : {{ $produk->gender }}</p>
                                                        <p>Harga : {{ number_format($produk->hargaJual) }}</p>
                                                        <div>
                                                            @if ($produk->jenis == 'Kambing')
                                                                <img class="rounded float-start"
                                                                    src="{{ asset('img/kambingg.jpg') }}">
                                                            @elseif($produk->jenis == 'Domba')
                                                                <img src="{{ asset('img/domba.jpg') }}">
                                                            @else
                                                                <img src="{{ asset('img/sapi.jpg') }}">
                                                            @endif
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Tutup</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div><!-- /.row -->
                </div>
            </section>

            <div id="carouselExampleControls" class="carousel slide py-5 my-3" data-ride="carousel">
                <h1 class="text-center py-2">#Testimoni</h1>
                <div class="container py-5 my-3">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <h3 class="py-4 text-center">James Rio</h3>
                                                <h5 class="py-2">Sangat Memuaskan pelayanan baik dan ramah serta hewannya
                                                    berkualitas</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <h3 class="py-4 text-center">Budi Sudirman</h3>
                                                <h5 class="py-2">Sangat Memuaskan pelayanan baik dan ramah serta hewannya
                                                    berkualitas</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <h3 class="py-4 text-center">Anne Marie</h3>
                                                <h5 class="py-2">Sangat Memuaskan pelayanan baik dan ramah serta hewannya
                                                    berkualitas</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </main>
@endsection
