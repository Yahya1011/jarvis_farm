@extends('layouts.master')

@section('title')
    Jarvis Farm
@endsection

@section('title-content')
    Farm
@endsection

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <h1 class="h1 pb-4 py-3">Hello, {{ $users->name }}</h1>
                    <div class="card mt-2">
                        <div class="card-header">{{ __('Data User') }}</div>
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('user.update', $users->id) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                @isset($users)
                                    {{ method_field('PATCH') }}
                                @endisset
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" name="name" class="form-control"
                                                value="{{ $users->name }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" name="email" class="form-control"
                                                value="{{ $users->email }}">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="role">Role</label>
                                            <select class="form-control form-control-sm" name="role" id="role">
                                                <option value="">{{ $users->role }}</option>
                                                <option value="admin">Admin</option>
                                                <option value="user">User</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <a href="{{ route('user.index') }}" type="submit" class="btn btn-primary">back</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
