@extends('layouts.master')

@section('title')
    RSUD Anugrah Sehat Afiat
@endsection()

@section('title-content')
    Data Pasien
@endsection()

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-3 text-gray-800">@yield('title-content')</h1>
    <div class="d-sm-flex align-items-center mb-2">
        <a href="{{ route('produk.create') }}"
            class="mr-3 d-none d-sm-inline-block btn btn-sm btn-outline-danger shadow-sm"><i
                class="fas fa-plus fa-sm text-white-50"></i> Pasien baru</a>
        <a href="{{ route('produk.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-outline-primary shadow-sm"><i
                class="fas fa-plus fa-sm text-white-50"></i> Pasien lama</a>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            {{-- <h6 class="m-0 font-weight-bold text-primary">Data Product</h6> --}}
        </div>
        <div class="card-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>jenis</th>
                            <th>type</th>
                            <th>berat</th>
                            <th>tinggi</th>
                            <th>gender</th>
                            <th>Harga</th>
                            <th>aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($produks as $key => $produk)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $produk->jenis }}</td>
                                <td>{{ $produk->type }}</td>
                                <td>{{ $produk->wight }} kg</td>
                                <td>{{ $produk->hight }} cm</td>
                                <td>{{ $produk->gender }}</td>
                                <td>Rp. {{ number_format($produk->hargaJual) }}</td>
                                <td>
                                    <a href="{{ route('produk.edit', $produk->id) }}" class="btn btn-primary btn-sm"><i
                                            class="fas fa-pencil-alt"></i></a>
                                    <a href="{{ route('produk.show', $produk->id) }}"
                                        class="btn btn-sm btn-primary rounded-0"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('produk.destroy', $produk->id) }}"
                                        onclick="return confirm('Apakah anda yakin untuk menghapus data ini?')"
                                        class="btn btn-sm btn-danger rounded-0"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection()
