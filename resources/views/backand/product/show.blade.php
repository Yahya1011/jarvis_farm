@extends('layouts.master')

@section('title')
Jarvis Farm
@endsection

@section('title-content')
Farm
@endsection

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<div class="container">
    <div class="col-md-12">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1 class="h1 pb-4 py-3">Data Produk</h1>
            <div class="card mt-2">
                <div class="card-header">{{ __('Data Produk') }}</div>
                <div class="card-body">
                    @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
                    <form action="{{route('produk.update', $produks->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @isset($produks)
                            {{ method_field('PATCH') }}
                        @endisset
                        <div class="row">
                        <div class="col-6">
                        <div class="form-group">
                        <label for="jenis">Jenis</label>
                        <select class="form-control form-control-sm" name="jenis">
                            <option selected="">{{ $produks->jenis }}</option>
                            <option value="Sapi">Sapi</option>
                            <option value="Kambing">Kambing</option>
                            <option value="Domba">Domba</option>
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select name="type" id="" class="form-control">
                                <option selected="" >{{ $produks->type }}</option>
                                <option value="Type A">type A</option>
                                <option value="Type B">type B</option>
                                <option value="Type C">type C</option>
                                <option value="Type D">type D</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="gender">Jenis Kelamin</label>
                            <select name="gender" id="" class="form-control">
                                <option selected="" >{{ $produks->gender }}</option>
                                <option value="Jantan">Jantan</option>
                                <option value="Betina">Betina</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" id="" class="form-control" value="{{old('status', $produks->status ?? '')}}">
                                <option value="" >Pilih Status</option>
                                <option value="Tersedia">Tersedia</option>
                                <option value="Kosong">Kosong</option>
                            </select>
                        </div>
                    </div>
                        <div class="col-6">
                        <div class="form-group">
                            <label for="hight">Tinggi</label>
                            <input type="text" name="hight" class="form-control" value="{{ old('hight', $produks->hight ?? '') }}">
                        </div>
                        <div class="form-group">
                            <label for="wight">Berat</label>
                            <input type="text" name="wight" class="form-control" value="{{ old('wight', $produks->wight ?? '') }}">
                        </div>
                        <div class="form-group">
                            <label for="hargaBeli">Harga Beli</label>
                            <input type="text" name="hargaBeli" class="form-control" value="{{ old('hargaBeli', $produks->hargaBeli ?? '') }}">
                        </div>
                        <div class="form-group">
                            <label for="hargaJual">Harga Jual</label>
                            <input type="text" name="hargaJual" class="form-control" value="{{ old('hargaJual', $produks->hargaJual ?? '') }}">
                        </div>
                        <div class="form-group">
                            <h6>Upload Image</h6>
                            <div class="controls">
                            <input type="file" name="imageName" class="form-control" id="imageName" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <img id="showImage" src="{{ (!empty($produks->imageName)) ? url('uploads/'.$produks->imageName):url('uploads/no_image.jpg') }}" style="width: 100px; width: 100px; border: 1px solid #000000;"> 
                            </div>
                         </div>
                    </div>
                </div>
                <br>
                    <a href="{{ route('produk.index') }}" type="button" class="btn btn-primary">Back</a>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#imageName').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#showImage').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>

@endsection