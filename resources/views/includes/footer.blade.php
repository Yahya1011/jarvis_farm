<!---------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------- Footer ----------------------------------------------------------------------------->
<footer class="page-footer container-fluid font-small py-4 bg-light">
    <div class="container pt-5">
        <div class="row">
            <div class="col-12 col-md-5 ">
                <img style="width: 25%;" src="{{ asset('img/logo.png') }}" alt="">
                <p class="lead p">Alamat Green Farm Kota Depok
                </p>
            </div>
            <div class="col-12 col-md-6 offset-md-1 justify-content-end pb-3">
                <div class="row">
                    <div class="col-6">
                        <div class="card shadow border-0">
                            <div class="card-body">
                                <span class="text-muted">Telepon</span>
                                <a href="tel:112">
                                    <h6 class="lora h6 pt-2"><i class="mr-2 fa fa-phone-alt"></i>+6221 77205320</h6>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card shadow border-0 pb-1">
                            <div class="card-body">
                                <span class="text-muted">Email kami</span>
                                <a href="mailto:portal@depok.go.id">
                                    <h6 class="lora h6 pt-2" style="font-size: 1rem"><i
                                            class="mr-2 far fa-envelope"></i>greenfarm@gmail.com</h6>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="container-fluid" style="background-color: #1D4F88">
    <div class="footer-copyright text-center py-3" style="color:#EAEAEA">© 2024 Green Farm</div>
</div>
