@extends('includes.layout')
@section('content')
    <main role="main">

        <div class="container-fluid">
            <section>
                <div class="container">
                    <!-- Three columns of text below the carousel -->
                    <div class="row py-5">
                        <h1 class=" col-12 py-5 text-black font-weight-bolder">#Daftar Produk</h1>
                        @foreach ($produks as $produk)
                            <div class="col-md-4 py-3">
                                <div class="card">
                                    @if ($produk->jenis == 'Kambing')
                                        <img src="{{ asset('img/kambingg.jpg') }}">
                                    @elseif($produk->jenis == 'Domba')
                                        <img src="{{ asset('img/domba.jpg') }}">
                                    @else
                                        <img src="{{ asset('img/sapi.jpg') }}">
                                    @endif

                                    <div class="card-body">
                                        <h5> {{ $produk->jenis }} {{ $produk->gender }} </h5>
                                        <p> {{ number_format($produk->hargaJual) }} </p>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-outline-info col-5" data-toggle="modal"
                                            data-target="#modal-detail-{{ $produk->id }}">
                                            Lihat detail
                                        </button>
                                        <button class="btn btn-outline-success col-5"> Beli </button>
                                        <div class="modal fade" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true"
                                            id="modal-detail-{{ $produk->id }}">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Detail {{ $produk->jenis }}
                                                            {{ $produk->gender }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Nama : {{ $produk->jenis }}</p>
                                                        <p>Type : {{ $produk->type }}</p>
                                                        <p>Berat : {{ $produk->wight }} kg</p>
                                                        <p>Tinggi : {{ $produk->hight }} cm</p>
                                                        <p>Jenis Kelamin : {{ $produk->gender }}</p>
                                                        <p>Harga : {{ number_format($produk->hargaJual) }}</p>
                                                        <div>
                                                            @if ($produk->jenis == 'Kambing')
                                                                <img class="rounded float-start"
                                                                    src="{{ asset('img/kambingg.jpg') }}">
                                                            @elseif($produk->jenis == 'Domba')
                                                                <img src="{{ asset('img/domba.jpg') }}">
                                                            @else
                                                                <img src="{{ asset('img/sapi.jpg') }}">
                                                            @endif
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Tutup</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div><!-- /.row -->
                </div>
            </section>


        </div>
    </main>
@endsection
