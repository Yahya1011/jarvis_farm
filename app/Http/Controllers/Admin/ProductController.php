<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produk;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk['produks'] = Produk::all();
        return view('backand.product.index', $produk);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backand.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        //$data = $request->all();


        //$data['imageName'] = $request->file('imageName')->store('img', 'public');

        //Produk::create($data);


        // dd($prouduct);


        $produks = new Produk;
        $produks->jenis = $request->jenis;
        $produks->type = $request->type;
        $produks->hight = $request->hight;
        $produks->wight = $request->wight;
        $produks->gender = $request->gender;
        $produks->hargaBeli = $request->hargaBeli;
        $produks->hargaJual = $request->hargaJual;
        $produks->status = $request->status;

        if ($request->file('imageName')) {
            $file = $request->file('imageName');
            $filename = date('YmdHi') . $file->getClientOriginalName();
            $file->move(public_path('uploads'), $filename);
            $produks['imageName'] = $filename;
        }
        //dd($produks);

        $produks->save();
        return back()->with('success', 'Data berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produks = Produk::findOrFail($id);
        return view('backand.product.show', compact([
            'produks'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produks = Produk::findOrFail($id);
        return view('backand.product.update', compact([
            'produks'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //  $data = $request->all();
        //  $data['imageName'] = $request->file('imageName')->store('img','public');

        // Produk::create($data); 
        $produks = Produk::findOrFail($id);
        $produks->jenis = $request->jenis;
        $produks->type = $request->type;
        $produks->hight = $request->hight;
        $produks->wight = $request->wight;
        $produks->gender = $request->gender;
        $produks->hargaJual = $request->hargaJual;
        $produks->hargaBeli = $request->hargaBeli;
        $produks->status = $request->status;
        // dd($produks);

        $produks->save();
        return back()->with('success', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $produk = Produk::find($id);
        $produk->delete();
        return redirect()->route('produk.index')->with('success', 'Data berhasil di hapus');
    }
}
