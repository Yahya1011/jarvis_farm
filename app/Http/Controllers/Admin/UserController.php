<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller

{
    public function index()
    {
        $data['users'] = User::all();
        return view('backand.user.index', $data);
    }

    public function create()
    {
        return view('backand.user.create');
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;
        //dd($user);
        $user->save();
        return view('backand.user.index');
    }

    public function show($id)
    {
        $users = User::findOrFail($id);
        return view('backand.user.show', compact([
            'users'
        ]));
    }

    public function edit($id)
    {
        $users = User::findOrFail($id);
        return view('backand.user.update', compact([
            'users'
        ]));
    }

    public function update(Request $request, $id)
    {
        $users = User::findOrFail($id);
        $users->name = $request->name;
        $users->email = $request->email;
        $users->role = $request->role;
        //dd($users);
        $users->save();

        return redirect()->route('user.index')->with('success', 'Data berhasil diupdate');
    }

    public function destroy($id)
    {
        $users = User::where('id', $id)->delete();
        //$users->delete();

        return redirect()->route('user.index')->with('success', 'Data berhasil dihapus');
    }
}
