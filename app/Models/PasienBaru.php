<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasienBaru extends Model
{
    use HasFactory;
    protected $table = 'pasien_baru';

    protected $fiilable = [
        'nik',
        'notlp',
        'namaLengkap',
        'pembayaran',
        'tempatLahir',
        'tanggalLahir',
        'jenisKelamin',
        'status',
        'agama',
        'pendidikan',
        'namaIbu',
        'pekerjaan',
        'alamat',
        'tanggalPeriksa',
        'poliklinik',
        'namaDokter',
        'norm'
    ];
}
