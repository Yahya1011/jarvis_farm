<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasienBaruTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasien_baru', function (Blueprint $table) {
            $table->id();
            $table->integer('nik');
            $table->string('notlp');
            $table->string('namaLengkap');
            $table->string('pembayaran');
            $table->string('tempatLahir');
            $table->date('tanggalLahir');
            $table->string('jenisKelamin');
            $table->string('status');
            $table->string('agama');
            $table->string('pendidikan');
            $table->string('namaIbu');
            $table->string('pekerjaan');
            $table->string('alamat');
            $table->date('tanggalPeriksa');
            $table->string('poliklinik');
            $table->string('namaDokter');
            $table->integer('norm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien_baru');
    }
}
