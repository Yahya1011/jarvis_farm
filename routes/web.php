<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [ProdukController::class, 'dataProduk']);


Route::prefix('admin')->namespace('Admin')->group(static function () {
        Route::resource('produk', 'ProductController');
        Route::resource('user', 'UserController');
});

Route::get('/delete/{id}', [UserController::class, 'destroy'])->name('userDelete');
Route::get('/delete/[id]', [ProductController::class, 'delete'])->name('delete');

// Route::get('user', [UserController::class, 'user']);
Route::get('formproduk', 'HomeController@formproduk')->name('formproduk');
Route::get('daftarbarang', [ProdukController::class, 'daftarBarang'])->name('daftarBarang');

// Route::get('depan',[ProdukController::class,'dataProduk']);
Route::POST('formproduk', [ProdukController::class, 'store']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::prefix('user')->group(function () {
//     Route::resource('produk', 'ProdukController');
// });